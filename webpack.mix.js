const mix = require('laravel-mix');
require('dotenv').config();
require('laravel-mix-polyfill');
const theme = process.env.WP_THEME;
mix.setResourceRoot('../');
mix.setPublicPath(`public/themes/${theme}/assets`);
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const imageminSvgo = require('imagemin-svgo');
const ImageminWebpWebpackPlugin= require("imagemin-webp-webpack-plugin");
mix.webpackConfig({
	module: {
		rules: [

			{
				test: /\.jsx?$/,
				exclude: /(node_modules\/(?!(dom7|swiper)\/).*|bower_components)/,
				use: [
					{
						loader: 'babel-loader',
						options: Config.babel()
					}
				],
				test: /\.mst$/,
				use: [
					{
						loader: 'mustache-loader',
					}
				]
			}
		]
	},
});
if(  process.env.NODE_ENV == 'production' ){
	mix.webpackConfig({
		plugins: [
			new ImageminPlugin({
				test: /\.(jpe?g)$/i,
				plugins: [
					imageminMozjpeg({
						quality: 90,
						progressive: true
					}),
				]
			}),
			new ImageminPlugin({
				test: /\.(png)$/i,
				plugins: [
					imageminPngquant({
						quality: [0.8, 0.9]
					}),
				]
			}),
			new ImageminPlugin({
				test: /\.(svg)$/i,
				plugins: [
					imageminSvgo({
						plugins: [
							{removeViewBox: false}
						]
					}),
				]
			}),
			new ImageminWebpWebpackPlugin({
				config: [{
			    test: /\.(jpe?g|png)/,
			    options: {
			      quality:  75
			    }
			  }],
			  overrideExtension: true,
			  detailedLogs: false,
			  strict: true
			}),

		]
	});
}
mix.webpackConfig({
	plugins: [
		new CopyWebpackPlugin([{
			from: `resources/images`,
			to: `images`, // Laravel mix will place this in 'public/img'
		}])
	]
});
mix.autoload({  // or Mix.autoload() ?
	'jquery': ['$', 'window.jQuery', 'jQuery'],
});
mix.browserSync({
	proxy: process.env.PROJECT_NAME,
	files: [
		`public/themes/${theme}/**/*.php`,
		`resources/**/*.js`,
		`resources/**/*.css`,
		`resources/**/*.jpg`,
		`resources/**/*.png`,
		`resources/**/*.svg`,
	]
});
mix.js('resources/scripts/app.js', 'scripts')
   .sass('resources/styles/app.scss', 'styles', {
			includePaths: ['node_modules', 'ressources'],
			implementation: require('node-sass')
		})
		.options({
			autoprefixer: {
				options: {
					grid: true,
				}
			}
		})
		.polyfill({
			enabled: true,
			useBuiltIns: "usage",
			targets: {"firefox": 40, "chrome":40, "ie": 11, "safari":8},
			corejs: 2,
			debug: true
		})
		.version();

mix.config.webpackConfig.output = {
	chunkFilename: 'scripts/[name].js',
	publicPath: '/themes/wordplate/assets/',
};
mix.version();
