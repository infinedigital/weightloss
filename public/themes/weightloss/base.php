<?php
get_template_part( 'templates/head' );
?>
<body <?php body_class( '' ); ?>>
	<div id="consent_blackbar"></div>
	<?php the_field('google_tag_noscript', 'option'); ?>
	
	<?php
	do_action( 'get_header' );
	get_template_part( 'templates/header' );
	?>
	<div id="swup" class="transition-fade">
	<?php include roots_template_path(); ?>
	</div><!-- /.wrap -->
	<?php get_template_part( 'templates/footer' ); ?>

<?php wp_footer(); ?>
</body>
</html>
