<?php

declare(strict_types=1);
//default disable XMLRPC
add_filter( 'xmlrpc_enabled', '__return_false' );
// Register plugin helpers.
require get_theme_file_path( 'includes/plugins/plate.php' );

add_image_size( 'product-image', 350, 350 );
// Set theme defaults.
add_action(
	'after_setup_theme',
	function () {
		// Disable the admin toolbar.
		show_admin_bar( false );
		load_theme_textdomain( 'zaffranax', get_template_directory() . '/languages' );
		// Add post thumbnails support.
		add_theme_support( 'post-thumbnails' );
		//add_editor_style( mix( 'styles/style-editor.css' ) );

		// Add title tag theme support.
		add_theme_support( 'title-tag' );

		// Add HTML5 theme support.
		add_theme_support(
			'html5',
			[
				'caption',
				'comment-form',
				'comment-list',
				'gallery',
				'search-form',
				'widgets',
			]
		);

		// Register navigation menus.
		register_nav_menus(
			array(
				'nav-primary' => __( 'nav-primary', 'roots' ),
			)
		);
		register_nav_menus(
			array(
				'nav-footer' => __( 'nav-footer', 'roots' ),
			)
		);
		register_nav_menus(
			array(
				'nav-links' => __( 'nav-links', 'roots' ),
			)
		);
	}
);

// Enqueue and register scripts the right way.
add_action(
	'wp_enqueue_scripts',
	function () {
		wp_deregister_script( 'wp-embed' );
		if ( ! is_page_template( 'page-buy-now.php' ) ) {
			wp_deregister_script( 'jquery' );
		}
		wp_enqueue_style( 'wordplate', get_theme_file_uri() . '/assets/styles/app.css' );
		wp_register_script( 'wordplate', get_theme_file_uri() . '/assets/scripts/app.js', '', '', true );
		wp_enqueue_script( 'wordplate' );
	}
);

// Remove JPEG compression.
add_filter(
	'jpeg_quality',
	function () {
		return 100;
	},
	10,
	2
);
if ( ! function_exists( 'glob_recursive' ) ) {
	// Does not support flag GLOB_BRACE
	function glob_recursive( $pattern, $flags = 0 ) {
		$files = glob( $pattern, $flags );
		foreach ( glob( dirname( $pattern ) . '/*', GLOB_ONLYDIR | GLOB_NOSORT ) as $dir ) {
			$files = array_merge( $files, glob_recursive( $dir . '/' . basename( $pattern ), $flags ) );
		}
		return $files;
	}
}
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
add_action( 'after_setup_theme', 'roots_setup' );
add_theme_support( 'plate-footer-text', 'Thank you for creating with In Fine.' );
add_theme_support( 'plate-disable-api' );
add_theme_support( 'plate-disable-tabs', [ 'help', 'screen-options' ] );
add_theme_support( 'plate-login-logo', sprintf( '%s/%s', get_template_directory_uri(), '/assets/images/logo-in_fine.svg' ) );
add_theme_support( 'align-wide' );
add_theme_support( 'disable-custom-colors' );

function disable_wp_emojicons() {
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}
add_action( 'init', 'disable_wp_emojicons' );
foreach ( glob_recursive( __DIR__ . '/lib/*.php' ) as $filename ) {
	require_once $filename;
}
