<?php /* Template Name: Home */ ?>

<?php
$logo_brand = get_field( 'logo_generic', 'options' );

$intro       = get_field( 'product_intro' );
$intro_title = get_field( 'intro_title' );
$intro_image = get_field( 'intro_image' );

$formula       = get_field( 'formula' );
$formula_image = get_field( 'formula_image' );
$products      = get_field( 'related_product' );

$ingredients_image = get_field( 'ingredients_image' );
$ingredients_title = get_field( 'ingredients_title' );
$ingredients_desc  = get_field( 'ingredients_desc' );
$clinic_image      = get_field( 'clinically_image' );
$clinic_title      = get_field( 'clinically_title' );
$clinic_desc       = get_field( 'clinically_desc' );
$advice            = get_field( 'advice' );
$remember          = get_field( 'remember' );
$discover          = get_field('discover', 'options');

$patchwork        = get_field( 'desktop_patchwork' );
$patchwork_tablet = get_field( 'tablet_patchwork' );
$patchwork_mobile = get_field( 'mobile_patchwork' );
?>
<!-- Main -->
<main class="main">
	<!-- Landing -->
	<!-- =================================== -->
	<section class="landing">
		<h1 class="landing__baseline" data-aos="fade-up" data-aos-delay="800" data-aos-duration="2000">
			<?php echo $intro_title; ?>
		</h1>
		<div class="landing__container container">
			<?php $i = 0;
			foreach ($products as $product ): $i++;
				$post = get_post( $product, OBJECT );
				$image_product  = get_field( 'header_image' );
				$image          = $image_product['image'];
				$header_content = get_field( 'header_content' );
				$color          = get_field( 'color' );
				if( $color == 'wl' ) {
					$id    = 'product-weight-loss';
					$hover = 'orange';
				} else{
					$id    = 'product-weight-loss-plus';
					$hover = 'fushia';
				}
			?>
			<div class="landing__item" data-aos-delay="<?php echo ( ($i * 100) + 300 ); ?>">
				<div class="landing__pack" data-aos="fade-up">
					<div class="pack">
						<picture>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>"<?php if( $image[ 'sizes' ][ 'large-height' ] ) { $size = $image[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
						</picture>
						<i class="pack__shadow"></i>
					</div>
				</div>
				<div class="landing__title-group" data-aos="fade-up" data-aos-delay="<?php echo ( ($i * 100) ); ?>">
					<h2 class="landing__title"><?php echo $header_content['name']; ?></h2>
					<h3 class="landing__subtitle"><?php echo $header_content['subname']; ?></h3>
				</div>
				<div class="landing__btn" data-aos="fade-up" data-aos-delay="<?php echo ( ($i * 200) ); ?>">
					<a href="#products" class="btn btn--lg btn--white<?php echo '-' . $color; ?>--to-<?php echo $hover; ?>" title="<?php echo $discover; ?>">
						<?php echo $discover; ?>
					</a>
				</div>
			</div>
			<?php
			endforeach;
			wp_reset_query();
			?>
		</div>
		<span class="landing__ground"></span>
	</section>

	<!-- Intro -->
	<!-- =================================== -->
	<section class="intro" data-aos-duration="1600">
		<div class="intro__container container">
			<div class="intro__content col">
				<div class="intro__title-group">
					<h3 class="intro__toptitle" data-aos="fade-up" data-aos-delay="300">
						<?php echo $intro['pre-title']; ?>
					</h3>
					<h2 class="intro__title" data-aos="fade-up" data-aos-delay="600">
						<?php echo $intro['title']; ?>
					</h2>
				</div>
				<p class="intro__text" data-aos="fade-up" data-aos-delay="600">
					<?php echo $intro['desc']; ?>
				</p>
				<a href="#products" class="btn btn--lg btn--orange--to-black" data-aos="zoom-out" data-aos-delay="600">
					<?php echo $intro['label']; ?>
				</a>
			</div>
			<div class="intro__illustration col">
				<div class="intro__image">
					<div class="intro__strong-point" data-aos="fade-left">
						<?php if ( $intro_image['text_image']) : ?>
						<img src="<?php echo $intro_image['text_image']['url']; ?>" alt="<?php echo $intro_image['text_image']['title']; ?>"<?php if( $intro_image['text_image'][ 'sizes' ][ 'large-height' ] ) { $size = $intro_image['text_image'][ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
						<?php else: ?>
						<img src="<?php bloginfo( 'url' ); ?>/themes/weightloss/assets/images/intro/xls-weightloss-take-it.svg" alt="Grab it, open it, take it">
						<?php endif; ?>
					</div>
					<picture>
						<img src="<?php echo $intro_image['pack_image']['url']; ?>" alt="<?php echo $intro_image['pack_image']['title']; ?>"<?php if( $intro_image['pack_image'][ 'sizes' ][ 'large-height' ] ) { $size = $intro_image['pack_image'][ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
					</picture>
				</div>
			</div>
		</div>
	</section>

	<!-- Key -->
	<!-- =================================== -->
	<section class="key">
		<div class="key__container container">
			<div class="key__badge">
				<img src="<?php echo $logo_brand['url']; ?>" alt="<?php echo $logo_brand['alt']; ?>" loading="lazy" data-aos="zoom-in"<?php if( $logo_brand[ 'sizes' ][ 'large-height' ] ) { $size = $logo_brand[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
			</div>
			<?php
			if( have_rows( 'formula' ) ):
				$i = 0;
				while ( have_rows( 'formula' ) ) : the_row();
			?>
			<div class="key__content col">
				<h2 class="key__title" data-aos="fade-right">
					<?php echo $formula['title']; ?>
				</h2>
				<?php if( have_rows( 'listing' ) ): ?>
				<div class="key__list">
					<ul class="checklist checklist--key">
						<?php
						while ( have_rows( 'listing' ) ) : the_row();
						$item = get_sub_field('listing-item'); $i++;
						?>
						<li class="checklist__item" data-aos="fade-right" data-aos-delay="<?php echo ( ($i * 50) ); ?>">
							<?php echo $item; ?>
						</li>
						<?php endwhile; ?>
					</ul>
				</div>
				<?php endif; ?>
			</div>
			<?php
				endwhile;
			endif;
			?>
			<?php if ( $formula_image ) : ?>
			<div class="key__hands col">
				<picture>
					<img src="<?php echo $formula_image['url']; ?>" alt="<?php echo $formula_image['title']; ?>" loading="lazy"<?php if( $formula_image[ 'sizes' ][ 'large-height' ] ) { $size = $formula_image[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
				</picture>
			</div>
			<?php endif; ?>
		</div>
	</section>

	<!-- Products -->
	<!-- =================================== -->
	<?php // Classes product--wl and product--wlp control colors and aspect ?>
	<section id="products" class="products">
		<?php
		foreach ($products as $product):
			$post = get_post( $product, OBJECT );
			$image_product = get_field( 'product-image' );
			$image_url     = wp_get_attachment_image_src( $image_product, 'thumbnail', false, array( 'alt' => get_the_title() ) );
			include( 'single-product.php' );
		endforeach;
		wp_reset_query();
		?>
	</section>

	<!-- Higlights -->
	<!-- =================================== -->
	<section class="highlights">
		<!-- Highlights staggered -->
		<div class="highlights__staggered container">
			<div class="staggered">
				<div class="staggered__illustration col" data-aos="fade-up">
					<picture>
						<img src="<?php echo $ingredients_image['url']; ?>" alt="<?php echo $ingredients_image['alt']; ?>" loading="lazy"<?php if( $ingredients_image[ 'sizes' ][ 'large-height' ] ) { $size = $ingredients_image[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
					</picture>
				</div>
				<div class="staggered__content col">
					<h4 class="staggered__title" data-aos="fade-up" data-aos-delay="200">
						<?php echo $ingredients_title; ?>
					</h4>
					<p class="staggered__text p" data-aos="fade-up" data-aos-delay="300">
						<?php echo $ingredients_desc; ?>
					</p>
				</div>
			</div>
			<div class="staggered">
				<div class="staggered__illustration col" data-aos="fade-up">
					<picture>
						<img src="<?php echo $clinic_image['url']; ?>" alt="<?php echo $clinic_image['alt']; ?>" loading="lazy"<?php if( $clinic_image[ 'sizes' ][ 'large-height' ] ) { $size = $clinic_image[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
					</picture>
				</div>
				<div class="staggered__content col">
					<h5 class="staggered__title" data-aos="fade-up" data-aos-delay="200">
						<?php echo $clinic_title; ?>
					</h5>
					<p class="staggered__text p" data-aos="fade-up" data-aos-delay="300">
						<?php echo $clinic_desc; ?>
					</p>
				</div>
			</div>
		</div>
		<!-- Highlights Cards -->
		<div class="highlights__cards">
			<div class="card__group">
				<div class="card" data-aos="zoom-out">
					<div class="card__inner">
						<div class="card__illustration">
							<picture>
								<img src="<?php echo $advice['image']['url']; ?>" alt="<?php echo $advice['image']['alt']; ?>" loading="lazy"<?php if( $advice['image'][ 'sizes' ][ 'large-height' ] ) { $size = $advice['image'][ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
							</picture>
						</div>
						<div class="card__title">
							<?php echo $advice['title']; ?>
						</div>
						<p class="card__body">
							<?php echo $advice['desc']; ?>
						</p>
					</div>
				</div>
				<div class="card" data-aos="zoom-out" data-aos-delay="300">
					<div class="card__inner">
						<div class="card__illustration">
							<picture>
								<img src="<?php echo $remember['image']['url']; ?>" alt="<?php echo $remember['image']['alt']; ?>" loading="lazy"<?php if( $remember['image'][ 'sizes' ][ 'large-height' ] ) { $size = $remember['image'][ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
							</picture>
						</div>
						<div class="card__title">
							<?php echo $remember['title']; ?>
						</div>
						<p class="card__body">
							<?php echo $remember['desc']; ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Patchwork -->
	<!-- =================================== -->
	<?php if( $patchwork ) : ?>
	<div class="patchwork">
		<picture class="patchwork__image">
			<source srcset="<?php echo $patchwork['url']; ?>" media="(min-width: 1140px)" type="image/jpeg">
			<?php if( $patchwork_tablet ) : ?>
			<source srcset="<?php echo $patchwork_tablet['url']; ?>" media="(min-width: 540px)" type="image/jpeg">
			<?php endif; ?>
			<?php if( $patchwork_mobile ) : ?>
			<source srcset="<?php echo $patchwork_mobile['url']; ?>" type="image/jpeg">
			<?php endif; ?>
			<img src="<?php echo $patchwork['url']; ?>" alt="<?php echo $patchwork['title']; ?>" loading="lazy"<?php if( $patchwork[ 'sizes' ][ 'large-height' ] ) { $size = $patchwork[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
		</picture>
	</div>
	<?php endif; ?>
</main>

<?php
foreach ($products as $product):
	$post = get_post( $product, OBJECT );
	$image_product = get_field( 'product-image' );
	$image_url     = wp_get_attachment_image_src( $image_product, 'thumbnail', false, array( 'alt' => get_the_title() ) );
	include( 'single-product-popup.php' );
endforeach;
wp_reset_query();
?>
