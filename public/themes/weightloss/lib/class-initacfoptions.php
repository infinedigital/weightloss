<?php
/**
* Adding ACF to Page Options
* @package physiomer
**/

if( function_exists('acf_add_options_page') ) :
class InitAcfOptions {
	public function init() {
		acf_add_options_page();
		add_action( 'init', array( $this, 'register_acf' ) );
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Options',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'normal',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
		acf_field_group(
			[
				'title'           => 'Other options',
				'fields'          => $this->register_reusable_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'normal',
				'label_placement' => 'top',
				'menu_order'      => 1,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'name'  => 'menu',
					'label' => 'Menu navigation',
				]
			),
			acf_text(
				[
					'name' => 'weight_loss',
					'label' => '"Weight loss" button',
					'instructions' => 'Fill this field if you have Weight Loss product',
					'placeholder' => 'XL-S Weight Loss',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_text(
				[
					'name' => 'weight_loss_plus',
					'label' => '"Weight loss +" button',
					'instructions' => 'Fill this field if you have Weight Loss + product',
					'placeholder' => 'XL-S Weight Loss +',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_text(
				[
					'name'         => 'xls_label',
					'label'        => '"XLS Medical" button',
					'instructions' => 'Translation of: XL-S Medical Website',
					'placeholder'  => 'XL-S Medical Website',
					'wrapper'      => [
						'width' => 50,
					],
				]
			),
			acf_link(
				[
					'name'         => 'xls_link',
					'label'        => '"XLS Medical" link',
					'instructions' => 'Link to your XL-S medical website',
					'wrapper'      => [
						'width' => 50,
					],
				]
			),
			acf_true_false(
				[
					'name'         => 'lng',
					'label'        => 'Multiple Languages',
					'instructions' => 'Do you need a lang switcher',
				]
			),
			acf_tab(
				[
					'name'  => 'generic_tab',
					'label' => 'Multiple used words',
				]
			),
			acf_text(
				[
					'name' => 'natural',
					'label' => 'Natural ingredients',
					'required' => true,
					'placeholder'  => 'Natural ingredients',
					'default_value' => 'Natural ingredients',
					'wrapper' => [
						'width' => 25,
					],
				]
			),
			acf_text(
				[
					'name'         => 'buynow',
					'label'        => 'Buy now label',
					'instructions' => 'Translation of : buy now',
					'default_value' => 'Buy now',
					'wrapper' => [
						'width' => 25,
					],
				]
			),
			acf_text(
				[
					'name'         => 'read',
					'label'        => 'Read the precautions label',
					'instructions' => 'Translation of : Read the precautions',
					'default_value' => 'Read the precautions',
					'wrapper' => [
						'width' => 25,
					],
				]
			),
			acf_text(
				[
					'name'         => 'discover',
					'label'        => 'Discover the product label',
					'instructions' => 'Translation of : Discover the product',
					'default_value' => 'Discover the product',
					'wrapper' => [
						'width' => 25,
					],
				]
			),
			acf_tab(
				[
					'name'  => 'footer_tab',
					'label' => 'Footer general',
				]
			),
			acf_image(
				[
					'name' => 'logo_generic',
					'label' => 'Weight loss Logo',
					'instructions' => 'Logo used in every area of the website and footer.',
					'return_format' => 'array',
					'wrapper' => [
						'width' => 30,
					],
				]
			),
			acf_group(
				[
					'name' => 'footer',
					'label' => 'Footer text',
					'wrapper' => [
						'width' => 70,
					],
					'instructions' => 'Copyright, address and generic legal mentions',
					'sub_fields' => [
						acf_text(
							[
								'name' => 'copy',
								'label' => 'Copyright generic text',
								'required' => true,
								'default_value' => 'Copywright 2020 PERRIGO - All right reserved',
							]
						),
						acf_text(
							[
								'name' => 'address',
								'label' => 'Address of the office',
								'placeholder' => 'Industrial Zoning De Prijkels - Venecoweg 26 9810 Nazareth - Belgium',
							]
						),
						acf_textarea(
							[
								'name' => 'footnote',
								'label' => 'Generic footnotes',
								'rows' => '4',
								'new_lines' => 'br',
								'instructions' => 'Mentions visible in every page. Use "enter" to start new line.',
							]
						),
					],
				]
			),
			acf_image(
				[
					'name' => 'logo_perrigo',
					'label' => 'Perrigo Logo',
					'return_format' => 'array',
					'wrapper' => [
						'width' => 30,
					],
				]
			),
			acf_link(
				[
					'name'         => 'link_perrigo',
					'label'        => 'Perrigo link',
					'instructions' => 'Link to Perrigo website',
					'wrapper'      => [
						'width' => 70,
					],
				]
			),
			acf_group(
				[
					'name' => 'legal',
					'label' => 'Cookies and terms',
					'required' => true,
					'instructions' => 'Cookies,terms and conditions labels in the footer',
					'sub_fields' => [
						acf_link(
							[
								'name' => 'cookies',
								'label' => 'Cookies policy',
								'instructions' => 'Link to : Privacy policy | Cookies preferences',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name' => 'privacy',
								'label' => 'Privacy statement',
								'instructions' => 'Link to : Privacy statement',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name' => 'terms',
								'label' => 'Terms & Conditions',
								'instructions' => 'Link to : Terms & Conditions',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
					],
				]
			),
			acf_text(
				[
					'name' => 'back_top',
					'label' => 'Back to top',
					'default_value' => 'Go to top',
					'instructions' => 'Label of the button linking to the top of the page',
				]
			),
			acf_tab(
				[
					'name'  => 'retailers_tab',
					'label' => 'Retailers',
				]
			),
			acf_text(
				[
					'name'          => 'retailers_title',
					'label'         => 'Retailers title',
					'instructions'  => htmlentities( 'Use <strong></strong> to set text in orange.' ),
					'default_value' => 'You can find XL-S WEIGHT LOSS in: ',
				]
			),
			acf_repeater(
				[
					'name'         => 'retailers',
					'label'        => 'Retailers',
					'layout'       => 'block',
					'instructions' => 'Insert the logo, link and google ID tracking if needed.',
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'logo',
								'label'         => 'Logo',
								'instructions'  => 'Please use format 148x80px',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name'     => 'link',
								'label'    => 'Retailer link',
								'required' => true,
								'wrapper'  => [
									'width' => 33,
								],
							]
						),
						acf_text(
							[
								'name'    => 'id',
								'label'   => 'Google tag ID',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name'  => '404_tab',
					'label' => '404 page',
				]
			),
			acf_text(
				[
					'name'          => 'title_404',
					'label'         => '404 page title',
					'required'      => true,
					'default_value' => 'The page you are looking for can not be found.',
					'instructions'  => 'Translation of: The page you are looking for can not be found.',
				]
			),
			acf_text(
				[
					'name'          => 'subtitle_404',
					'label'         => '404 question',
					'required'      => true,
					'default_value' => 'While we have you, did you know that',
					'instructions'  => 'Type a few words about this brand.',
				]
			),
			acf_textarea(
				[
					'name'          => 'desc_404',
					'label'         => 'Fun fact',
					'rows'          => '3',
					'instructions'  => 'Second part of your introduction if wanted',
				]
			),
			acf_link(
				[
					'name'          => 'first_link_404',
					'label'         => 'Link to main page',
					'instructions'  => 'Link to the main page you would like to send.',
					'wrapper'       => [
						'width' => 50,
					],
				]
			),
			acf_link(
				[
					'name'          => 'second_link_404',
					'label'         => 'Link to second page',
					'instructions'  => 'Usually link to the homepage',
					'wrapper'       => [
						'width' => 50,
					],
				]
			),
		];
		return $base_fields;
	}
	private function register_reusable_fields() {
		$base_fields = [
			acf_textarea(
				[
					'name' => 'google_tag',
					'label' => 'Google tag code',
					'rows' => '5',
					'instructions' => 'Insert complete Google Tag Manager code. Please do not add the noscript tag. Comments can be removed. This code goes as high in the <head> of the page as possible.',
					'placeholder' => '<script></script>'
				]
			),
			acf_textarea(
				[
					'name' => 'script_tag',
					'label' => 'Additional script',
					'rows' => '2',
					'instructions' => 'Insert some script if needed, like facebook pixel or something else.',
				]
			),
			acf_textarea(
				[
					'name' => 'google_tag_noscript',
					'label' => 'No script code for Google Tag',
					'rows' => '2',
					'instructions' => 'This code goes immediately after the opening <body> tag.',
					'placeholder' => '<noscript></noscript>'
				]
			),
			acf_textarea(
				[
					'name' => 'cookies_script',
					'label' => 'Cookie consent script',
					'rows' => '3',
					'instructions' => 'This code goes right after the css.',
					'placeholder' => '<script></script>'
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'options_page', '==', 'acf-options' ),
			],
		];
		return $location;
	}
}
$acf_home = new InitAcfOptions();
$acf_home->init();
endif;