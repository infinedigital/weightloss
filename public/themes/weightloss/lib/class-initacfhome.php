<?php
/**
* Adding ACF to Page Home
* @package physiomer
**/

if( function_exists('acf_add_options_page') ) :
class InitAcfHome {
	public function init() {
		acf_add_options_page();
		add_action( 'init', array( $this, 'register_acf' ) );
		add_action(
			'init', function () {
				remove_post_type_support( 'page', 'editor' );
				remove_post_type_support( 'page', 'comments' );
				remove_post_type_support( 'page', 'author' );
				remove_post_type_support( 'page', 'slug' );
				remove_post_type_support( 'page', 'excerpt' );
				remove_post_type_support( 'page', 'revisions' );
			}
		);
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Home',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'label' => 'Intro',
					'name'  => 'intro_tabs',
				]
			),
			acf_text(
				[
					'name'          => 'intro_title',
					'label'         => 'Main page title',
					'default_value' => 'It works, Simply',
					'placeholder'   => 'it works, Simply',
				]
			),
			acf_group(
				[
					'name'         => 'product_intro',
					'label'        => 'Product intro',
					'instructions' => '',
					'wrapper'      => [
						'width'  => 70,
					],
					'sub_fields'  => [
						acf_text(
							[
								'name'          => 'pre-title',
								'label'         => 'Pre-title',
								'instructions'  => 'Small title before de main title. Translation of: Our product help you to lose weight',
								'default_value' => 'Our product help you to lose weight',
							]
						),
						acf_text(
							[
								'name'          => 'title',
								'label'         => 'Main title',
								'instructions'  => 'Translation of : So far losing weight did not work how you wanted?',
								'default_value' => 'So far losing weight did not work how you wanted?',
							]
						),
						acf_textarea(
							[
								'name'         => 'desc',
								'label'        => 'Small description',
								'instructions' => htmlentities('A few words about the product. Use <strong></strong> to set text in bold black.'),
								'rows'         => '3',
							]
						),
						acf_text(
							[
								'name'          => 'label',
								'label'         => 'Discover label',
								'instructions'  => 'Label displayed in the button. Translation of : Discover our product',
								'default_value' => 'Discover our product',
							]
						),
					],
				]
			),
			acf_group(
				[
					'name'          => 'intro_image',
					'label'         => 'Intro images',
					'wrapper'       => [
						'width' => 30,
					],
					'instructions' => 'Please contact In Fine if you need another image.',
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'pack_image',
								'label'         => 'Flower and pack image',
								'instructions'  => 'Add the image with one or two product.',
								'return_format' => 'array',
								'required'     => true,
							]
						),
						acf_image(
							[
								'name'          => 'text_image',
								'label'         => 'Grab it, take it',
								'instructions'  => 'Add the "Grab it, take it" text image in png.',
								'return_format' => 'array',
							]
						),
					],
				]
			),
			acf_tab(
				[
					'label' => 'Products',
					'name'  => 'products_tab',
				]
			),
			acf_image(
				[
					'name' => 'formula_image',
					'label' => 'Hands with pack',
					'required' => true,
					'instructions' => 'Select the image with the pack handle in hands. Contact In Fine if you need another image.',
					'wrapper'  => [
						'width' => 30,
					],
				]
			),
			acf_group(
				[
					'name'  => 'formula',
					'label' => 'Our formula',
					'instructions' => 'Title and list about the formula',
					'required' => true,
					'wrapper'  => [
						'width' => 70,
					],
					'sub_fields' => [
						acf_text(
							[
								'name'  => 'title',
								'label' => 'Formula title',
								'default_value' => 'Our formula',
							]
						),
						acf_repeater(
							[
								'name' => 'listing',
								'label' => 'Listing',
								'instructions' => htmlentities( 'Use <strong></strong> to set text in bold. Add a row for each line of the formula.' ),
								'sub_fields' => [
									acf_text(
										[
											'name'  => 'listing-item',
											'label' => 'Formula item',
											'placeholder'  => 'For exemple: clinically proven',
										]
									),
								],
							]
						),
					],
				]
			),
			acf_relationship(
				[
					'name'          => 'related_product',
					'label'         => 'Select your products',
					'instructions'  => 'Select the products to display',
					'post_type'     => [
						'product',
					],
					'required'      => false,
					'return_format' => 'id',
				]
			),
			acf_tab(
				[
					'label' => 'Ingredients',
					'name'  => 'ingredients_tab',
				]
			),
			acf_text(
				[
					'name'          => 'ingredients_title',
					'label'         => 'Ingredients title',
					'instructions'  => 'Translation of: Naturally sourced principal ingredients',
					'required'      => true,
					'default_value' => 'Naturally sourced principal ingredients',
				]
			),
			acf_image(
				[
					'name'          => 'ingredients_image',
					'label'         => 'Ingredients image',
					'required'      => true,
					'wrapper'  => [
						'width' => 40,
					],
				]
			),
			acf_textarea(
				[
					'name'          => 'ingredients_desc',
					'label'         => 'Ingredients description',
					'required'      => true,
					'wrapper'  => [
						'width' => 60,
					],
					'new_lines'     => 'br',
					'rows'          => '3',
					'default_value' => 'Our formula is made with a carefully selected natural fibres from acacia gum and Mediterranean Nopal leaves. ',
				]
			),
			acf_text(
				[
					'name'          => 'clinically_title',
					'label'         => 'Clinically proven title',
					'instructions'  => 'Translation of: Clinically proven',
					'required'      => true,
					'default_value' => 'Clinically proven',
				]
			),
			acf_image(
				[
					'name'          => 'clinically_image',
					'label'         => 'Clinically proven image',
					'required'      => true,
					'wrapper'  => [
						'width' => 40,
					],
				]
			),
			acf_textarea(
				[
					'name'          => 'clinically_desc',
					'label'         => 'Clinically proven description',
					'required'      => true,
					'new_lines'     => 'br',
					'rows'          => '3',
					'default_value' => 'Our clinically proven formula effectively reduces the absorption of dietary fats, avoiding their excessive build-up in your body. Its effective results have been published in clinical study.  ',
				]
			),
			acf_group(
				[
					'name'         => 'advice',
					'label'        => 'Advice card',
					'required'     => true,
					'wrapper'      => [
						'width' => 50,
					],
					'instructions' => 'Card with icon, title and few words intro. Please contact In Fine if you need another icon.',
					'sub_fields'   => [
						acf_image(
							[
								'name'          => 'image',
								'label'         => 'Advice image',
								'required'      => true,
							]
						),
						acf_text(
							[
								'name'          => 'title',
								'label'         => 'Advice title',
								'default_value' => 'Advice',
							]
						),
						acf_textarea(
							[
								'name'          => 'desc',
								'label'         => 'Advice description',
								'rows'          => '2',
								'new_lines'     => 'br',
								'default_value' => 'Stay hydrated. Drink at least two litres of water a day. ',
							]
						),
					],
				]
			),
			acf_group(
				[
					'name'         => 'remember',
					'label'        => 'Remember card',
					'required'     => true,
					'instructions' => 'Card with icon, title and few words intro. Please contact In Fine if you need another icon.',
					'wrapper'      => [
						'width' => 50,
					],
					'sub_fields'   => [
						acf_image(
							[
								'name'          => 'image',
								'label'         => 'Remember image',
								'required'      => true,
							]
						),
						acf_text(
							[
								'name'          => 'title',
								'label'         => 'Remember title',
								'default_value' => 'Remember',
							]
						),
						acf_textarea(
							[
								'name'          => 'desc',
								'label'         => 'Remember description',
								'rows'          => '2',
								'new_lines'     => 'br',
								'default_value' => 'Maintain an active and healthy lifestyle.',
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name'  => 'patchwork_tab',
					'label' => 'Patchwork banner',
				]
			),
			acf_image(
				[
					'name'          => 'desktop_patchwork',
					'label'         => 'Desktop patchwork image',
					'instructions'  => 'Use size 1400 x 560px. Please contact In Fine if you need to change the image.',
					'return_format' => 'array',
					'wrapper'       => [
						'width' => 33,
					],
				]
			),
			acf_image(
				[
					'name'          => 'tablet_patchwork',
					'label'         => 'Tablet patchwork image',
					'instructions'  => 'Use size 1140 x 840px. Please contact In Fine if you need to change the image.',
					'return_format' => 'array',
					'wrapper'       => [
						'width' => 33,
					],
				]
			),
			acf_image(
				[
					'name'          => 'mobile_patchwork',
					'label'         => 'Mobile patchwork image',
					'instructions'  => 'Use size 760 x 840px. Please contact In Fine if you need to change the image.',
					'return_format' => 'array',
					'wrapper'       => [
						'width' => 33,
					],
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'post_type', '==', 'page' ),
			],
		];
		return $location;
	}
}
$acf_home = new InitAcfHome();
$acf_home->init();
endif;