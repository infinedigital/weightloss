<?php

function self_customizer_section( $wp_customize ) {
	$wp_customize->add_section(
		'wordplate',
		array(
			'title'       => __( 'WordPlate options', 'xls' ),
			'description' => __( 'theme general options', 'xls' ),
		)
	);
}

add_action( 'customize_register', 'self_customizer_section' );
