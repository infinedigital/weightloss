<?php
/**
* Adding ACF to Page Home
* @package physiomer
**/

if( function_exists('acf_add_options_page') ) :
class InitAcfProduct {
	public function init() {
		acf_add_options_page();
		add_action( 'init', array( $this, 'register_acf' ) );
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Product',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'label' => 'Product',
					'name'  => 'product_tabs',
				]
			),
			acf_select(
				[
					'name' => 'color',
					'label' => 'Color related to product',
					'instructions' => 'Select the color related to your product.',
					'choices'       => [
						'wl'  => 'Orange: for Weight loss',
						'wlp'   => 'Rose: for Weight loss +',
					],
					'default_value' => [
						'wl',
					],
				]
			),
			acf_group(
				[
					'name'       => 'header_image',
					'label'      => 'Product image',
					'wrapper'    => [
						'width' => 40,
					],
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'image',
								'label'         => 'Main image',
								'instructions'  => 'Image displayed in the single page. Please contact In Fine if you need to change the image.',
								'return_format' => 'array',
								'required'      => true,
							]
						),
					],
				]
			),
			acf_group(
				[
					'name'       => 'header_content',
					'label'      => 'Product content',
					'wrapper'    => [
						'width' => 60,
					],
					'sub_fields' => [
						acf_text(
							[
								'name'       => 'name',
								'label'      => 'Product name'
							]
						),
						acf_text(
							[
								'name'       => 'subname',
								'label'      => 'Sub product name'
							]
						),
						acf_wysiwyg(
							[
								'name'         => 'description',
								'label'        => 'Product introduction',
								'toolbar'      => 'basic',
								'media_upload' => false,
								'instructions' => 'Please use current text and bold text.',
							]
						),
						acf_textarea(
							[
								'name'         => 'more_description',
								'label'        => 'More product introduction',
								'rows'         => '3',
								'instructions' => htmlentities('Use <strong></strong> to set text in orange. Example: <strong>XL-S Weight loss</strong> has effective results which havebeen published in clinical study'),
							]
						),
						acf_repeater(
							[
								'name'       => 'list_ingredient',
								'label'      => 'Natural principal ingredients',
								'layout'     => 'block',
								'instructions' => 'Insert all the ingredients',
								'sub_fields' => [
									acf_text(
										[
											'name'    => 'ingredient',
											'label'   => 'Ingredient',
										]
									),
								],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'label' => 'How to use',
					'name'  => 'use_tab',
				]
			),
			acf_repeater(
				[
					'name'       => 'howtouse_icons',
					'label'      => 'List links',
					'layout'     => 'block',
					'instructions' => 'Insert here the "How to use" icons in the logical order. Please contact In Fine if you need another icon.',
					'wrapper'    => [
						'width' => 50,
					],
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'image',
								'label'         => 'Icon: Noon, morning or evening',
								'instructions'  => 'Do not forget to include a alt attribute to the image.',
								'return_format' => 'array',
							]
						),
					],
				]
			),
			acf_wysiwyg(
				[
					'name'         => 'description',
					'label'        => 'How to use introduction',
					'toolbar'      => 'basic',
					'media_upload' => false,
					'instructions' => 'Please use current text and bold text.',
					'wrapper'    => [
						'width' => 50,
					],
				]
			),
			acf_tab(
				[
					'label' => 'Read de precaution',
					'name'  => 'precaution_tab',
				]
			),
			acf_group(
				[
					'name'       => 'precaution',
					'label'      => 'Product content',
					'sub_fields' => [
						acf_text(
							[
								'name'       => 'name',
								'label'      => 'Product name'
							]
						),
						acf_wysiwyg(
							[
								'name'         => 'description',
								'label'        => 'Precaution content',
								'toolbar'      => 'basic',
								'media_upload' => false,
								'instructions' => 'Please use current text and bold text.',
							]
						),
						acf_wysiwyg(
							[
								'name'         => 'footer',
								'label'        => 'Precaution footer',
								'toolbar'      => 'basic',
								'media_upload' => false,
								'instructions' => 'Please use current text and bold text.',
							]
						),
					],
				]
			),
			acf_tab(
				[
					'label' => 'Retailers',
					'name'  => 'retailers_tab',
				]
			),
			acf_repeater(
				[
					'name'       => 'retailers',
					'label'      => 'Retailers list',
					'layout'     => 'table',
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'retailer_logo',
								'label'         => 'Retailer logo',
								'instructions'  => 'Please use format same format',
								'return_format' => 'array',
							]
						),
						acf_url(
							[
								'name'          => 'retailer_link',
								'label'         => 'Retailer link',
							]
						),
					],
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'post_type', '==', 'product' ),
			],
		];
		return $location;
	}
}
$acf_product = new InitAcfProduct();
$acf_product->init();
endif;