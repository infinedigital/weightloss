<?php
/* template single product */
/* Define variables */
$color          = get_field( 'color' );
$header_image   = get_field( 'header_image' );
$image          = $header_image['image'];
$header_content = get_field( 'header_content' );
$precaution     = get_field( 'precaution' );
$howtouse_desc  = get_field( 'description' );

$natural        = get_field( 'natural', 'options' );
$precautions    = get_field( 'read', 'options' );
$buynow         = get_field( 'buynow', 'options' );
if($color == 'wl') {
	$id = 'product-weight-loss';
} else{
	$id = 'product-weight-loss-plus';
}
?>
<div id="<?php echo $id; ?>" class="product product-<?php echo '-' . $color; ?> container">
	<div class="product__pack col">
		<div class="product__tabs">
			<img src="<?php bloginfo( 'url' ); ?>/themes/weightloss/assets/images/tabs/xls-weightloss<?php echo '-' . $color; ?>-tabs.png" alt="" loading="lazy">
		</div>
		<div class="product__pack__image">
			<div class="pack pack--product">
				<picture>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>" loading="lazy"<?php if( $image[ 'sizes' ][ 'large-height' ] ) { $size = $image[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
				</picture>
				<i class="pack__shadow"></i>
			</div>
		</div>
	</div>
	<div class="product__content col">
		<div class="product__title-group">
			<h3 class="product__title" data-aos="fade-up" data-aos-delay="200">
				<?php echo $header_content['name']; ?>
			</h3>
			<h4 class="product__subtitle" data-aos="fade-up" data-aos-delay="400">
				<?php echo $header_content['subname']; ?>
			</h4>
		</div>
		<div class="product__text" data-aos="fade-up" data-aos-delay="200">
			<?php echo $header_content['description']; ?>
		</div>
		<div class="product__text product__text--small" data-aos="fade-up">
			<p><?php echo $header_content['more_description']; ?></p>
		</div>
		<div class="product__ingredients" data-aos="fade-up">
			<div class="block">
				<div class="block__title">
					<?php echo $natural; ?>
				</div>
				<?php
				if( have_rows( 'header_content' ) ):
					while ( have_rows( 'header_content' ) ) : the_row();
						if( have_rows( 'list_ingredient' ) ):
				?>
				<div class="block__checklist">
					<ul class="checklist checklist--inline">
						<?php
						while ( have_rows( 'list_ingredient' ) ) : the_row();
							$item = get_sub_field('ingredient');
						?>
							<li class="checklist__item">
								<?php echo $item; ?>
							</li>
						<?php endwhile; ?>
					</ul>
				</div>
				<?php 
						endif;
					endwhile;
				endif; 
				?>
			</div>
		</div>
	</div>
	<div class="product__dosage">
		<div class="product__dosage__buttons">
			<span data-target="#shops--wl_<?php echo $id; ?>" class="popup__trigger btn btn--black--to<?php echo '-' . $color; ?>" title="Buy now" data-aos="zoom-out">
				<?php echo $buynow; ?>
			</span>
			<span data-target="#precautions--wl_<?php echo $id; ?>" class="popup__trigger btn btn--sm btn--white<?php echo '-' . $color; ?>--to<?php echo '-' . $color; ?>" title="Read the precautions">
				<?php echo $precautions; ?>
			</span>
		</div>
		<?php if( have_rows( 'howtouse_icons' ) ): ?>
		<div class="product__dosage__schema">
			<?php
			while ( have_rows( 'howtouse_icons' ) ) : the_row();
				$item = get_sub_field( 'image' );
			?>
			<div class="product__dosage__item">
				<img src="<?php echo $item['url']; ?>" alt="<?php echo $item['title']; ?>" loading="lazy">
			</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
		<div class="product__dosage__advice">
			<?php echo $howtouse_desc; ?>
		</div>
	</div>
</div>