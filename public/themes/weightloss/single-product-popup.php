
<?php
/* template single product */
/* Define variables */
$color          = get_field( 'color' );
$header_image   = get_field( 'header_image' );
$image          = $header_image['image'];
$header_content = get_field( 'header_content' );
$precaution     = get_field( 'precaution' );
$howtouse_desc  = get_field( 'description' );

$natural        = get_field( 'natural', 'options' );
$precautions    = get_field( 'read', 'options' );
$buynow         = get_field( 'buynow', 'options' );
if($color == 'wl') {
	$id = 'product-weight-loss';
} else{
	$id = 'product-weight-loss-plus';
}
?>
<div id="precautions--wl_<?php echo $id; ?>" class="precautions-popup">
	<div class="popup">
		<div class="popup__inner">
			<div class="popup__close"></div>
			<div class="popup__header">
				<div class="popup__title">
					<?php echo $precautions; ?>
				</div>
				<div class="popup__subtitle"><?php echo $precaution['name'] ?></div>
			</div>
			<div class="popup__content content">
				<?php echo $precaution['description'] ?>
			</div>
			<div class="popup__footer content">
				<?php echo $precaution['footer'] ?>
			</div>
		</div>
	</div>
</div>

<div id="shops--wl_<?php echo $id; ?>" class="shops-popup">
	<div class="popup">
		<div class="popup__inner">
			<div class="popup__close"></div>
			<div class="popup__header popup__header--center">
				<div class="popup__title">
					<?php echo $buynow; ?>
				</div>
				<div class="popup__subtitle"><?php echo $precaution['name'] ?></div>
			</div>
			<?php if( have_rows( 'retailers' ) ): ?>
			<div class="popup__content">
				<div class="popup__shops">
					<?php 
					while( have_rows( 'retailers' ) ): the_row(); 
						$logo = get_sub_field( 'retailer_logo' );
						$link = get_sub_field( 'retailer_link' );
					?>
					<a href="<?php echo $link; ?>" class="popup__shop" title="<?php echo $buynow; ?>" target="_blank" rel="noreferrer">
						<div class="popup__shop-logo">
							<img src="<?php echo $logo[ 'url' ]; ?>" alt="<?php echo $logo[ 'title' ]; ?>" loading="lazy">
						</div>
						<span class="popup__shop-btn">
							<?php echo $buynow; ?>
						</span>
					</a>
					<?php endwhile; ?>
				</div>
			</div>
			<?php else: ?>
			<div class="popup__content content">
				<p><?php _e('Sorry, no results were found.', 'roots'); ?></p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>