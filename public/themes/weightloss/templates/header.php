<?php
	$logo_brand = get_field( 'logo_generic', 'options' );
	$wl         = get_field( 'weight_loss', 'options' );
	$wlp        = get_field( 'weight_loss_plus', 'options' );
	$xls_label  = get_field( 'xls_label', 'options' );
	$xls_link   = get_field( 'xls_link', 'options' );
	$buynow     = get_field( 'buynow', 'options' );
	$discover   = get_field('discover', 'options');
	$products   = get_field( 'related_product' );
	$lng         = get_field( 'lng', 'options' );
?>

<header class="header">
	<a href="<?php echo bloginfo('url'); ?>" class="header__logo">
		<img src="<?php echo $logo_brand['url']; ?>" alt="<?php echo $logo_brand['title']; ?>" data-aos="flip-left" data-aos-delay="300"<?php if( $logo_brand[ 'sizes' ][ 'large-height' ] ) { $size = $logo_brand[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
	</a>
	<div class="navbar" data-aos="fade-down">
		<div class="navbar__menu">
			<nav class="navbar__nav">
				<?php if ( $wl ) : ?>
					<a href="#product-weight-loss" class="navbar__link" title="<?php echo $wl; ?>">
						<?php echo $wl; ?>
					</a>
				<?php
				endif;
				if ( $wlp ) : ?>
					<a href="#product-weight-loss-plus" class="navbar__link" title="<?php echo $wlp; ?>">
						<?php echo $wlp; ?>
					</a>
				<?php endif; ?>
			</nav>
			<div class="navbar__btn">
				<a href="#shops" class="navbar__btn-buy btn btn--sm btn--black--to-red" title="<?php echo $buynow; ?>">
					<?php echo $buynow; ?>
				</a>
				<?php if ( $xls_link && $xls_label ) : ?>
				<a href="<?php echo $xls_link['url']; ?>" class="navbar__btn-discover btn btn--sm btn--red--to-black" title="<?php echo $xls_label; ?>"  target="_blank" rel="noreferrer">
					<?php echo $xls_label; ?>
				</a>
				<?php endif; ?>
			</div>
			<?php 
			if ($lng) : 
				$translations = pll_the_languages(array( 'raw' => 1 ));
			?>
			<div class="navbar__lang">
				<span class="lang__btn"><?php echo pll_current_language(); ?></span>
				<ul class="lang">
					<?php foreach ( $translations as $lng ) : ?>
					<li class="lang__item">
						<a href="<?php echo $lng[ 'url' ] ?>" class="lang__link" title="<?php echo $lng[ 'name' ] ?>">
							<?php echo $lng[ 'name' ] ?>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php endif; ?>
		</div>
		<div class="navbar__trigger">
			<div id="hamburger-navbar" class="hamburger">
				<div class="hamburger__lines">
					<i class="hamburger__line hamburger__line--1"></i>
					<i class="hamburger__line hamburger__line--2"></i>
					<i class="hamburger__line hamburger__line--3"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="navscreen">
		<div class="navscreen__inner">
			<div class="navscreen__header">
				<?php 
				if ($lng) : 
					$translations = pll_the_languages(array( 'raw' => 1 ));
				?>
				<div class="navscreen__lang">
					<span class="lang__btn"><?php echo pll_current_language(); ?></span>
					<ul class="lang">
						<?php foreach ( $translations as $lng ) : ?>
						<li class="lang__item">
							<a href="<?php echo $lng[ 'url' ] ?>" class="lang__link" title="<?php echo $lng[ 'name' ] ?>">
								<?php echo $lng[ 'name' ] ?>
							</a>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>

				<div class="navscreen__trigger">
					<div id="hamburger-navscreen" class="hamburger">
						<div class="hamburger__lines">
							<i class="hamburger__line hamburger__line--1"></i>
							<i class="hamburger__line hamburger__line--2"></i>
							<i class="hamburger__line hamburger__line--3"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="navscreen__body">
				<?php
				foreach ($products as $product ):
					$post           = get_post( $product, OBJECT );
					$image_product  = get_field( 'header_image' );
					$image          = $image_product['image'];
					$header_content = get_field( 'header_content' );
					$color          = get_field( 'color' );
					if($color == 'wl') {
						$id = 'product-weight-loss';
					} else{
						$id = 'product-weight-loss-plus';
					}
				?>
				<a href="<?php echo '#' . $id; ?>"  class="navscreen__product" title="<?php echo $header_content['name']; ?>">
					<div class="navscreen__pack pack">
						<picture>
							<?php /*<source srcset="<?php bloginfo( 'url' ); ?>/themes/weightloss/assets/images/packs/xls-weightloss-fat-white.webp, <?php bloginfo( 'url' ); ?>/themes/weightloss/assets/images/packs/xls-weightloss-fat-white@2x.webp 2x" type="image/webp">
							<source srcset="<?php bloginfo( 'url' ); ?>/themes/weightloss/assets/images/packs/xls-weightloss-fat-white.png" type="image/png">
							*/ ?>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
						</picture>
						<i class="pack__shadow"></i>
					</div>
					
					<span class="navscreen__title"><?php echo $header_content['name']; ?></span>
					
					<span class="btn btn--sm btn--white-<?php echo $color; ?>--to-<?php echo $color; ?>" title="<?php echo $discover; ?>">
						<?php echo $discover; ?>
					</span>
				</a>
				<?php
					endforeach;
					wp_reset_query();
				?>
			</div>
			<div class="navscreen__footer">
				<a href="#products" class="btn btn--sm btn--black--to-red" title="<?php echo $buywnow; ?>">
					<?php echo $buynow; ?>
				</a>
				<?php if ( $xls_link && $xls_label ) : ?>
				<a href="<?php echo $xls_link['url']; ?>" class="btn btn--sm btn--red--to-black" title="<?php echo $xls_label; ?>"  target="_blank" rel="noreferrer">
					<?php echo $xls_label; ?>
				</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>
