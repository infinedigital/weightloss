<?php header( 'X-UA-Compatible: IE=edge' ); ?>
<!doctype html>
<?php if ( stripos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 10' ) ) : ?>
<html class="ie ie10" <?php language_attributes(); ?>>
<?php elseif ( stripos( $_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0' ) ) : ?>
<html class="ie ie11" <?php language_attributes(); ?>>
<?php elseif ( stripos( $_SERVER['HTTP_USER_AGENT'], 'Chrome' ) == false && stripos( $_SERVER['HTTP_USER_AGENT'], 'Safari' ) !== false ) : ?>
<html class="safari no-js" <?php language_attributes(); ?>>
<?php elseif ( stripos( $_SERVER['HTTP_USER_AGENT'], 'Firefox' ) ) : ?>
<html class="firefox no-js" <?php language_attributes(); ?>>
<?php else : ?>
<html class="no-js" <?php language_attributes(); ?>>
<?php endif; ?>
<head>
	<meta charset="utf-8">
	<meta name="theme-color" content="#032B76">
	<link rel="manifest" href="<?php bloginfo('template_directory'); ?>/manifest.json">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="<?php echo site_url(); ?>" />
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">
	
	<link rel="preload" href="https://p.typekit.net" crossorigin>
	<link rel="preload" href="<?php bloginfo('template_directory'); ?>/assets/styles/app.css" as="style">

	<?php
		$script  = get_field('script_tag', 'options');
		$tag     = get_field('google_tag', 'options');
		$cookies = get_field('cookies_script', 'options');
		echo $tag;
		echo $script;
	?>
	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://use.typekit.net/ama2fvu.css" async>
	<?php if( $cookies == '' ) : ?>
	<script src="//consent.trustarc.com/notice?domain=perrigo.com&amp;c=teconsent&amp;js=bb&amp;noticeType=bb&amp;text=true&amp;cookieLink=https%3A%2F%2Fwww.perrigo.com%2Fprivacy.aspx&amp;privacypolicylink=https%3A%2F%2Fwww.perrigo.com%2Fprivacy.aspx" async></script>
	<?php endif; ?>
</head>
