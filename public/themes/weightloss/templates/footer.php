<?php
	$retailers_title = get_field( 'retailers_title', 'options' );
	$legal           = get_field( 'legal', 'options' );
	$footer          = get_field( 'footer', 'options' );
	$logo_brand      = get_field( 'logo_generic', 'options' );
	$logo_perrigo    = get_field( 'logo_perrigo', 'options' );
	$link_perrigo    = get_field( 'link_perrigo', 'options' );
	$top             = get_field( 'back_top', 'options' );
?>
<!-- Footer -->
<footer class="footer">
	<!-- First Footer Section -->
	<div class="footer__shops">
		<div id="shops" class="shops container">
			<div class="shops__cta col">
				<?php echo $retailers_title; ?>
			</div>
			<?php
			if( have_rows( 'retailers', 'options' ) ) :
				?>
				<ul class="shops__list col">
					<?php
					while ( have_rows( 'retailers', 'options' ) ) : the_row();
					$logo = get_sub_field( 'logo' );
					$link = get_sub_field( 'link' );
					$link_target = $link['target'] ? $link['target'] : '_self';
					$id   = get_sub_field( 'id' );
						?>
						<li class="shops__item">
							<a <?php if($id) { echo 'id="' . $id . '"'; } ?> href="<?php echo $link['url']; ?>" class="shops__link" title="<?php echo $link['title']; ?>" target="<?php echo esc_attr( $link_target ); ?>" rel="noreferrer">
								<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['title']; ?>"<?php if( $logo[ 'sizes' ][ 'large-height' ] ) { $size = $logo[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
							</a>
						</li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
	</div>
	<!-- Second Footer Section -->
	<div class="footer__informations">
		<div class="footer__informations__inner container">
			<div class="footer__totop">
				<a href="#swup" class="btn btn--light btn--sm" title="<?php echo $top; ?>">
					<i class="icon icon--long-arrow-up icon--btn"></i>
					<?php echo $top; ?>
				</a>
			</div>
			<div class="footer__informations__wl col">
				<div class="footer__informations__logo">
					<img src="<?php echo $logo_brand['url']; ?>" alt="<?php echo $logo_brand['title']; ?>" loading="lazy"<?php if( $logo_brand[ 'sizes' ][ 'large-height' ] ) { $size = $logo_brand[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
				</div>
				<div class="footer__informations__mentions">
					<p class="footer__informations__copyright">
						<?php echo $footer['copy']; ?>
					</p>
					<p class="footer__informations__contact">
						<?php echo $footer['address']; ?>
					</p>
					<p class="footer__informations__references">
						<?php echo $footer['footnote']; ?>
					</p>
				</div>
			</div>
			<div class="footer__informations__perrigo col">
				<?php if($link_perrigo) : ?>
				<a href="<?php echo $link_perrigo['url']; ?>" title="<?php echo $link_perrigo['title']; ?>" target="_blank">
					<img src="<?php echo $logo_perrigo['url']; ?>" alt="<?php echo $logo_perrigo['title']; ?>" loading="lazy"<?php if( $logo_perrigo[ 'sizes' ][ 'large-height' ] ) { $size = $logo_perrigo[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
				</a>
				<?php else : ?>
				<a href="https://www.perrigo.com/" title="Perrigo" target="_blank" rel="noreferrer">
					<img src="<?php echo $logo_perrigo['url']; ?>" alt="<?php echo $logo_perrigo['title']; ?>" loading="lazy"<?php if( $logo_perrigo[ 'sizes' ][ 'large-height' ] ) { $size = $logo_perrigo[ 'sizes' ]; echo ' height="'. $size[ 'large-height' ] .'" width="'. $size[ 'large-width' ] .'"'; } ?>>
				</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<!-- Last Footer Section -->
	<div class="footer__legal">
		<ul class="footer__legal__list container">
			<li class="footer__legal__item">
				<a href="<?php echo $legal['cookies']['url']; ?>" class="footer__legal__link" target="_blank" title="<?php echo $legal['cookies']['title']; ?>" rel="noreferrer">
					<?php echo $legal['cookies']['title']; ?>
				</a>
			</li>
			<li class="footer__legal__item">
				<a href="<?php echo $legal['privacy']['url']; ?>" class="footer__legal__link" target="_blank" title="<?php echo $legal['privacy']['title']; ?>" rel="noreferrer">
					<?php echo $legal['privacy']['title']; ?>
				</a>
			</li>
			<li class="footer__legal__item">
				<a href="<?php echo $legal['terms']['url']; ?>" class="footer__legal__link" target="_blank" title="<?php echo $legal['terms']['title']; ?>" rel="noreferrer">
					<?php echo $legal['terms']['title']; ?>
				</a>
			</li>
		</ul>
	</div>
	<div id="teconsent"></div>
</footer>

<!--IE11-Popup-->
<script>
	var $buoop = {required:{e:-1},mobile:false,style:"bottom",api:2020.07 };
	function $buo_f(){
		var e = document.createElement("script");
		e.src = "//browser-update.org/update.min.js";
		document.body.appendChild(e);
	};
	try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
	catch(e){window.attachEvent("onload", $buo_f)}
</script>