<?php
$debug     = true;
$cmd       = isset( $_GET['all'] ) ? 'PURGEALL' : 'PURGE';
$hostname  = isset( $_GET['host'] ) ? $_GET['host'] : $_SERVER['HTTP_HOST'];
$path      = isset( $_GET['path'] ) ? $_GET['path'] : '';
$purge_url = 'http://' . $hostname . '/$path';

purge_url( $cmd, $purge_url, $debug );

function purge_url( $cmd, $purge_url, $debug ) {
	$fd               = false;
	$curl_potion_list = array(
		CURLOPT_RETURNTRANSFER    => true,
		CURLOPT_CUSTOMREQUEST     => $cmd,
		CURLOPT_URL               => $purge_url,
		CURLOPT_CONNECTTIMEOUT_MS => 2000,
	);
	if ( $debug ) {
		print '\n---- Curl debug -----\n';
		$fd                                  = fopen( 'php://output', 'w+' );
		$curl_potion_list[ CURLOPT_VERBOSE ] = true;
		$curl_potion_list[ CURLOPT_STDERR ]  = $fd;
	}
	$curl_handler = curl_init();
	curl_setopt_array( $curl_handler, $curl_potion_list );
	$data = curl_exec( $curl_handler );
	curl_close( $curl_handler );
	if ( false !== $fd ) {
		fclose( $fd );
	}
	print( $data );
}
