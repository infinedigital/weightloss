#!/bin/bash
# on est sur un serveur gandi
if [ -d "/srv/data/web/vhosts/weightloss.in-fine.be/public/themes/weightloss/" ]; then
	echo "********************************"
	echo "*            GANDI             *"
	echo "********************************"
	# b2h
	if [ -f /srv/data/tmp/weightloss-corpo ]
	then
		echo "********************************"
		echo "*           DEV               *"
		echo "********************************"
		rm htdocs
		ln -s public htdocs
		pwd
		ls -all
		cp /srv/data/web/vhosts/weightloss.in-fine.be/public/themes/weightloss/assets.zip public/themes/weightloss/assets.zip
		unzip -o public/themes/weightloss/assets.zip -d  public/themes/weightloss/
		rm -rf /srv/data/web/vhosts/weightloss.in-fine.be/public/themes/*
		rm public/.htaccess
		cp /srv/data/web/vhosts/weightloss.in-fine.be/public/.htaccess.prod  public/.htaccess
	fi
	echo "********************************"
	echo "*           CLEANUP            *"
	echo "********************************"
	rm -rf resources .babelrc .editorconfig .env.dev .env.example .env.prod .git-ftp-ignore .git-ftp-include  .gitignore LICENSE README.md Vagrantfile bitbucket-pipelines.yml post-install.sh provision.sh resources/ scotchbox.sql webpack.mix.*
	echo "********************************"
	echo "*        PURGE CACHE           *"
	echo "********************************"
	php /srv/data/tmp/purge.php
fi
