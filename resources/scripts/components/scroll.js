import Component from 'gia/Component';
export default class Scroll extends Component {
	unmount() {
		this.element.removeEventListener('click', this.scrollto, false)
	}
	mount() {
		this.element.addEventListener("click", this.scrollto, false);
	}
	scrollto( e ) {
		e.preventDefault();
		document.querySelector(this.getAttribute('href')).scrollIntoView({
			behavior: 'smooth'
		});
	}
}
