import Scroll from './scroll';
import Rgpd from './rgpd';
const components = {
	Scroll,
	Rgpd
}
export default components;
