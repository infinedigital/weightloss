import Component from 'gia/Component';
import Cookies from "js-cookie";
import "custom-event-polyfill";
import "mdn-polyfills/Node.prototype.remove";
var gdprCookieNoticeLocales = {
	en: {
		description: 'We use cookies to offer you a better browsing experience, personalise content and ads, to provide social media features and to analyse our traffic. Read about how we use cookies and how you can control them by clicking Cookie Settings. You consent to our cookies if you continue to use this website.',
		settings: 'Cookie settings',
		accept: 'I Accept',
		refuse: 'I refuse',
		statement: 'Our cookie statement',
		save: 'Save settings',
		always_on: 'Always on',
		cookie_essential_title: 'Essential website cookies',
		cookie_essential_desc: 'Necessary cookies help make a website usable by enabling basic functions like page navigation and access to secure areas of the website. The website cannot function properly without these cookies.',
		cookie_performance_title: 'Performance cookies',
		cookie_performance_desc: 'These cookies are used to enhance the performance and functionality of our websites but are non-essential to their use. For example it stores your preferred language or the region that you are in.',
		cookie_analytics_title: 'Analytics cookies',
		cookie_analytics_desc: 'We use analytics cookies to help us measure how users interact with website content, which helps us customize our websites and application for you in order to enhance your experience.',
		cookie_marketing_title: 'Marketing cookies',
		cookie_marketing_desc: 'These cookies are used to make advertising messages more relevant to you and your interests. The intention is to display ads that are relevant and engaging for the individual user and thereby more valuable for publishers and third party advertisers.'
	}
}

window["gdpr-cookie-notice-templates"] = {};
window["gdpr-cookie-notice-templates"]["bar.html"] = "<div class=\"gdpr-cookie-notice container-fluid\">\n" +
"  <p class=\"gdpr-cookie-notice-description col-md-10 col-lg-9 col-xl-9\">{description} <a href=\"#\" class=\"gdpr-cookie-notice-nav-item gdpr-cookie-notice-nav-item-settings\">{settings}</a></p>\n" +
"  <nav class=\"gdpr-cookie-notice-nav col-md-2 col-lg-3 col-xl-3\">\n" +

"    <div><a href=\"#\" class=\"gdpr-cookie-notice-nav-item gdpr-cookie-notice-nav-item-accept gdpr-cookie-notice-nav-item-btn\">{accept}</a>\n" +
"    <a href=\"#\" class=\"gdpr-cookie-notice-nav-item gdpr-cookie-notice-nav-item-refuse gdpr-cookie-notice-nav-item-btn\">{refuse}</a> </div>\n" +
"  </nav>\n" +
"</div>\n" +
"";

window["gdpr-cookie-notice-templates"]["category.html"] = "<li class=\"gdpr-cookie-notice-modal-cookie\">\n" +
"  <div class=\"gdpr-cookie-notice-modal-cookie-row\">\n" +
"    <h3 class=\"gdpr-cookie-notice-modal-cookie-title\">{title}</h3>\n" +
"    <input type=\"checkbox\" name=\"gdpr-cookie-notice-{prefix}\" checked=\"checked\" id=\"gdpr-cookie-notice-{prefix}\" class=\"gdpr-cookie-notice-modal-cookie-input\">\n" +
"    <label class=\"gdpr-cookie-notice-modal-cookie-input-switch\" for=\"gdpr-cookie-notice-{prefix}\"></label>\n" +
"  </div>\n" +
"  <p class=\"gdpr-cookie-notice-modal-cookie-info\">{desc}</p>\n" +
"</li>\n" +
"";

window["gdpr-cookie-notice-templates"]["modal.html"] = "<div class=\"gdpr-cookie-notice-modal\">\n" +
"  <div class=\"gdpr-cookie-notice-modal-content\">\n" +
"    <div class=\"gdpr-cookie-notice-modal-header\">\n" +
"      <h2 class=\"gdpr-cookie-notice-modal-title\">{settings}</h2>\n" +
"      <button type=\"button\" class=\"gdpr-cookie-notice-modal-close\"></button>\n" +
"    </div>\n" +
"    <ul class=\"gdpr-cookie-notice-modal-cookies\"></ul>\n" +
"    <div class=\"gdpr-cookie-notice-modal-footer\">\n" +
"      <a href=\"#\" class=\"gdpr-cookie-notice-modal-footer-item gdpr-cookie-notice-modal-footer-item-statement\">{statement}</a>\n" +
"      <a href=\"#\" class=\"gdpr-cookie-notice-modal-footer-item gdpr-cookie-notice-modal-footer-item-save gdpr-cookie-notice-modal-footer-item-btn\"><span>{save}</span></a>\n" +
"    </div>\n" +
"  </div>\n" +
"</div>\n" +
"";
window["gdpr-cookie-notice-templates"] = {};
window["gdpr-cookie-notice-templates"]["bar.html"] = "<div class=\"gdpr-cookie-notice container-fluid\">\n" +
"  <p class=\"gdpr-cookie-notice-description col-md-10 col-lg-9 col-xl-9\">{description} <a href=\"#\" class=\"gdpr-cookie-notice-nav-item gdpr-cookie-notice-nav-item-settings\">{settings}</a></p>\n" +
"  <nav class=\"gdpr-cookie-notice-nav col-md-2 col-lg-3 col-xl-3\">\n" +

"    <div><a href=\"#\" class=\"gdpr-cookie-notice-nav-item gdpr-cookie-notice-nav-item-accept gdpr-cookie-notice-nav-item-btn\">{accept}</a>\n" +
"    <a href=\"#\" class=\"gdpr-cookie-notice-nav-item gdpr-cookie-notice-nav-item-refuse gdpr-cookie-notice-nav-item-btn\">{refuse}</a> </div>\n" +
"  </nav>\n" +
"</div>\n" +
"";

window["gdpr-cookie-notice-templates"]["category.html"] = "<li class=\"gdpr-cookie-notice-modal-cookie\">\n" +
"  <div class=\"gdpr-cookie-notice-modal-cookie-row\">\n" +
"    <h3 class=\"gdpr-cookie-notice-modal-cookie-title\">{title}</h3>\n" +
"    <input type=\"checkbox\" name=\"gdpr-cookie-notice-{prefix}\" checked=\"checked\" id=\"gdpr-cookie-notice-{prefix}\" class=\"gdpr-cookie-notice-modal-cookie-input\">\n" +
"    <label class=\"gdpr-cookie-notice-modal-cookie-input-switch\" for=\"gdpr-cookie-notice-{prefix}\"></label>\n" +
"  </div>\n" +
"  <p class=\"gdpr-cookie-notice-modal-cookie-info\">{desc}</p>\n" +
"</li>\n" +
"";

window["gdpr-cookie-notice-templates"]["modal.html"] = "<div class=\"gdpr-cookie-notice-modal\">\n" +
"  <div class=\"gdpr-cookie-notice-modal-content\">\n" +
"    <div class=\"gdpr-cookie-notice-modal-header\">\n" +
"      <h2 class=\"gdpr-cookie-notice-modal-title\">{settings}</h2>\n" +
"      <button type=\"button\" class=\"gdpr-cookie-notice-modal-close\"></button>\n" +
"    </div>\n" +
"    <ul class=\"gdpr-cookie-notice-modal-cookies\"></ul>\n" +
"    <div class=\"gdpr-cookie-notice-modal-footer\">\n" +
"      <a href=\"#\" class=\"gdpr-cookie-notice-modal-footer-item gdpr-cookie-notice-modal-footer-item-statement\">{statement}</a>\n" +
"      <a href=\"#\" class=\"gdpr-cookie-notice-modal-footer-item gdpr-cookie-notice-modal-footer-item-save gdpr-cookie-notice-modal-footer-item-btn\"><span>{save}</span></a>\n" +
"    </div>\n" +
"  </div>\n" +
"</div>\n" +
"";




function gdpr(config) {
	var namespace = 'gdprcookienotice';
	var pluginPrefix = 'gdpr-cookie-notice';
	var templates = window[pluginPrefix+'-templates'];
	//  var gdprCookies = Cookies.noConflict();
	var modalLoaded = false;
	var noticeLoaded = false;
	var cookiesAccepted = false;
	var categories = ['performance', 'analytics', 'marketing'];

	// Default config options
	if(!config.locale) config.locale = 'en';
	if(!config.timeout) config.timeout = 500;
	if(!config.domain) config.domain = null;
	if(!config.expiration) config.expiration = 30;

	// Get the users current cookie selection
	var currentCookieSelection = getCookie();
	var cookiesAcceptedEvent = new CustomEvent('gdprCookiesEnabled', {detail: currentCookieSelection});

	// Show cookie bar if needed
	if(!currentCookieSelection) {
		console.log("show");
		showNotice();
	} else {
		console.log("hide");
		deleteCookies(currentCookieSelection);
		document.dispatchEvent(cookiesAcceptedEvent);
	}

	// Get gdpr cookie notice stored value
	function getCookie() {
		return Cookies.getJSON(namespace);
	}

	// Delete cookies if needed
	function deleteCookies(savedCookies) {
		var notAllEnabled = false;
		for (var i = 0; i < categories.length; i++) {
			if(config[categories[i]] && !savedCookies[categories[i]]) {
				for (var ii = 0; ii < config[categories[i]].length; ii++) {
					Cookies.remove(config[categories[i]][ii]);
					notAllEnabled = true;
				}
			}
		}

		// Show the notice if not all categories are enabled
		if(notAllEnabled) {
			console.log('not enabled')
			//  showNotice();
		} else {
			hideNotice();
		}
	}

	// Hide cookie notice bar
	function hideNotice() {
		document.documentElement.classList.remove(pluginPrefix+'-loaded');
	}

	// Write gdpr cookie notice's cookies when user accepts cookies
	function acceptCookies(save) {
		var value = {
			date: new Date(),
			necessary: true,
			performance: true,
			analytics: true,
			marketing: true
		};

		// If request was coming from the modal, check for the settings
		if(save) {
			for (var i = 0; i < categories.length; i++) {
				value[categories[i]] = document.getElementById(pluginPrefix+'-cookie_'+categories[i]).checked;
			}
		}
		Cookies.set(namespace, value, { expires: config.expiration, domain: config.domain });
		deleteCookies(value);

		// Load marketing scripts that only works when cookies are accepted
		cookiesAcceptedEvent = new CustomEvent('gdprCookiesEnabled', {detail: value});
		document.dispatchEvent(cookiesAcceptedEvent);

	}
	function refuseCookies(save) {
		var value = {
			date: new Date(),
			necessary: true,
			performance: false,
			analytics: false,
			marketing: false
		};

		// If request was coming from the modal, check for the settings
		if(save) {
			for (var i = 0; i < categories.length; i++) {
				value[categories[i]] = document.getElementById(pluginPrefix+'-cookie_'+categories[i]).checked = false;
			}
		}
		Cookies.set(namespace, value, { expires: config.expiration, domain: config.domain });
		deleteCookies(value);

		// Load marketing scripts that only works when cookies are accepted
		cookiesAcceptedEvent = new CustomEvent('gdprCookiesEnabled', {detail: value});
		document.dispatchEvent(cookiesAcceptedEvent);
		hideNotice();

	}

	// Show the cookie bar
	function buildNotice() {
		if(noticeLoaded) {
			return false;
		}

		var noticeHtml = localizeTemplate('bar.html');
		document.body.insertAdjacentHTML('beforeend', noticeHtml);

		// Load click functions
		setNoticeEventListeners();

		// Make sure its only loaded once
		noticeLoaded = true;
	}

	// Show the cookie notice
	function showNotice() {
		buildNotice();

		// Show the notice with a little timeout
		setTimeout(function(){
			document.documentElement.classList.add(pluginPrefix+'-loaded');
		}, config.timeout);
	}

	// Localize templates
	function localizeTemplate(template, prefix) {
		var str = templates[template];
		var data = gdprCookieNoticeLocales[config.locale];

		if(prefix) {
			prefix = prefix+'_';
		} else {
			prefix = '';
		}

		if (typeof str === 'string' && (data instanceof Object)) {
			for (var key in data) {
				return str.replace(/({([^}]+)})/g, function(i) {
					var key = i.replace(/{/, '').replace(/}/, '');

					if(key == 'prefix') {
						return prefix.slice(0, -1);
					}

					if(data[key]) {
						return data[key];
					} else if(data[prefix+key]) {
						return data[prefix+key];
					} else {
						return i;
					}
				});
			}
		} else {
			return false;
		}
	}

	// Build modal window
	function buildModal() {
		if(modalLoaded) {
			return false;
		}

		// Load modal template
		var modalHtml = localizeTemplate('modal.html');

		// Append modal into body
		document.body.insertAdjacentHTML('beforeend', modalHtml);

		// Get empty category list
		var categoryList = document.querySelector('.'+pluginPrefix+'-modal-cookies');

		//Load essential cookies
		categoryList.innerHTML += localizeTemplate('category.html', 'cookie_essential');
		var input = document.querySelector('.'+pluginPrefix+'-modal-cookie-input');
		var label = document.querySelector('.'+pluginPrefix+'-modal-cookie-input-switch');
		label.innerHTML = gdprCookieNoticeLocales[config.locale]['always_on'];
		label.classList.add(pluginPrefix+'-modal-cookie-state');
		label.classList.remove(pluginPrefix+'-modal-cookie-input-switch');
		input.remove();

		// Load other categories if needed
		if(config.performance) categoryList.innerHTML += localizeTemplate('category.html', 'cookie_performance');
		if(config.analytics) categoryList.innerHTML += localizeTemplate('category.html', 'cookie_analytics');
		if(config.marketing) categoryList.innerHTML += localizeTemplate('category.html', 'cookie_marketing');

		// Load click functions
		setModalEventListeners();

		// Update checkboxes based on stored info(if any)
		if(currentCookieSelection) {
			document.getElementById(pluginPrefix+'-cookie_performance').checked = currentCookieSelection.performance;
			document.getElementById(pluginPrefix+'-cookie_analytics').checked = currentCookieSelection.analytics;
			document.getElementById(pluginPrefix+'-cookie_marketing').checked = currentCookieSelection.marketing;
		}

		// Make sure modal is only loaded once
		modalLoaded = true;
	}

	// Show modal window
	function showModal() {
		buildModal();
		document.documentElement.classList.add(pluginPrefix+'-show-modal');
	}

	// Hide modal window
	function hideModal() {
		document.documentElement.classList.remove(pluginPrefix+'-show-modal');
	}

	// Click functions in the notice
	function setNoticeEventListeners() {
		var settingsButton = document.querySelectorAll('.'+pluginPrefix+'-nav-item-settings')[0];
		var acceptButton = document.querySelectorAll('.'+pluginPrefix+'-nav-item-accept')[0];
		var refuseButton = document.querySelectorAll('.'+pluginPrefix+'-nav-item-refuse')[0];

		settingsButton.addEventListener('click', function(e) {
			e.preventDefault();
			showModal();
		});

		acceptButton.addEventListener('click', function(e) {
			e.preventDefault();
			acceptCookies();
		});
		refuseButton.addEventListener('click', function(e) {
			e.preventDefault();
			refuseCookies(false);
		});

	}

	// Click functions in the modal
	function setModalEventListeners() {
		var closeButton = document.querySelectorAll('.'+pluginPrefix+'-modal-close')[0];
		var statementButton = document.querySelectorAll('.'+pluginPrefix+'-modal-footer-item-statement')[0];
		var categoryTitles = document.querySelectorAll('.'+pluginPrefix+'-modal-cookie-title');
		var saveButton = document.querySelectorAll('.'+pluginPrefix+'-modal-footer-item-save')[0];

		closeButton.addEventListener('click', function() {
			hideModal();
			return false;
		});

		statementButton.addEventListener('click', function(e) {
			e.preventDefault();
			window.location.href = config.statement;
		});

		for (var i = 0; i < categoryTitles.length; i++) {
			categoryTitles[i].addEventListener('click', function() {
				this.parentNode.parentNode.classList.toggle('open');
				return false;
			});
		}

		saveButton.addEventListener('click', function(e) {

			e.preventDefault();
			saveButton.classList.add('saved');
			setTimeout(function(){
				saveButton.classList.remove('saved');
			}, 1000);
			hideModal();
			hideNotice();

			acceptCookies(true);
		});

	}

	// Settings button on the page somewhere
	var globalSettingsButton = document.querySelectorAll('.'+pluginPrefix+'-settings-button');
	if(globalSettingsButton) {
		for (var i = 0; i < globalSettingsButton.length; i++) {
			globalSettingsButton[i].addEventListener('click', function(e) {
				e.preventDefault();
				showModal();
			});
		}
	}
}
export default class Scroll extends Component {
	mount() {
		gdpr({
			locale: 'en', //This is the default value
			timeout: 500, //Time until the cookie bar appears
			expiration: 30, //This is the default value, in days
			domain: window.location.hostname, //If you run the same cookie notice on all subdomains, define the main domain starting with a .
			statement: '/privacy-policy/', //Link to your cookie statement page
			performance: ['JSESSIONID'], //Cookies in the performance category.
			analytics: ['ga'], //Cookies in the analytics category.
			marketing: ['SSID'] //Cookies in the marketing category.
		});
	}
}
