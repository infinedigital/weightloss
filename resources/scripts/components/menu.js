import Component from 'gia/Component';

export default class Menu extends Component {
	unmount() {
	}
	mount() {
		let navprimary = document.getElementsByClassName("nav-bar");
		let height= navprimary[0].clientHeight;
		let offsettop= navprimary[0].offsetTop;
		let menutop= offsettop + height;
		let header = document.getElementById("top");
		if(!header.classList.contains('menu--top')){
			header.classList.add("menu--top");
		}
		window.onscroll = function() {scrollTopx(menutop)};
		var navicon = document.getElementById("navicon");
		var menu = document.getElementById("nav-mobile");
		navicon.onclick = function(){
			navicon.classList.toggle("close");
			menu.classList.toggle("open");
			document.body.classList.toggle("open");
		};

		function scrollTopx(menutop){
			let navprimary = document.getElementsByClassName("nav-bar");
			let height= navprimary[0].clientHeight;
			let header = document.getElementById("top");
			if(document.documentElement.scrollTop>menutop){
				header.setAttribute("style", "padding-top: "+height+"px");
				navprimary[0].setAttribute("style", "margin-top: -"+height+"px");
				if(header.classList.contains('menu--top')){
					header.classList.add("menu--fixed");
					header.classList.remove("menu--top");
				}
			}else{
				if(!header.classList.contains('menu--top')){
					header.classList.add("menu--top");
					header.classList.remove("menu--fixed");
					header.setAttribute("style", "");
					navprimary[0].setAttribute("style", "transform:translateY(0); transition: transform .2s ease-out;");
				}
			}
		}
	}
}
