import AOS from 'aos';
import components from './components';
import loadComponents from 'gia/loadComponents';
//import removeComponents from 'gia/removeComponents';
let aos = AOS.init({
	once: true,
	duration: 2000,
});

loadComponents(components);


$( '.navbar__trigger' ).click(function() {
	$( 'body' ).toggleClass( 'nav-open' );
});

$( '.navscreen__trigger' ).click(function() {
	$( 'body' ).toggleClass( 'nav-open' );
});

$( '.navscreen__product' ).click(function() {
	$( 'body' ).toggleClass( 'nav-open' );
});

$( '.popup__trigger' ).click(function() {
	var target = $(this).data( 'target' );

	$( target + ' .popup' ).toggleClass( 'popup--open' );
	$( 'body' ).toggleClass( 'popup-open' );
});

$( '.popup__close' ).click(function() {
	$( this ).parents( '.popup' ).toggleClass( 'popup--open' );
	$( 'body' ).toggleClass( 'popup-open' );
});

var hrefs = document.querySelectorAll('a[href^="#"]');

for (var i=0; i<hrefs.length; i++) {
	hrefs[i].addEventListener('click', function (e) {
		e.preventDefault();
		document.querySelector(this.getAttribute('href')).scrollIntoView({
			behavior: 'smooth'
		});
	});
}

//add IE modal
if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )){
	$('#modal').toggleClass('active')
	$('.toggleModal').on('click', function (e) {
		$('#modal').toggleClass('active');
	});
}

